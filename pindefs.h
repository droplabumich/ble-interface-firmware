
#define STATUS_LED p4 // GPIO_27
#define CAMERA_TRIGGER p12 //GPIO_3
// Strobe_trigger NOT CONNECTED in Top Board rev 1.0, use CAMERA_TRIGGER instead
#define STROBE_TRIGGER p13 //    NINA_B1_GPIO_4

#define POWER_3V3_ENABLE p14 	// NINA_B1_GPIO_5
#define POWER_5V_ENABLE p29 	// NINA_B1_GPIO_17
#define ESC_ENABLE p30			// NINA_B1_GPIO_18
#define ALTIMETER_ENABLE p11	// NINA_B1_GPIO_2
#define CAMERAS_ENABLE p28		// NINA_B1_GPIO_1

#define CHARGER_SHUTDOWN p16	// NINA_B1_GPIO_7
#define CHARGER_SENSE p28		// NINA_B1_GPIO_16

#define MOSI p31				// NINA_B1_GPIO_20
#define MISO p7					// NINA_B1_GPIO_21
#define SCK p16					// NINA_B1_GPIO_22
#define CS p5 					// NINA_B1_GPIO_23

#define TX p2 					// NINA_B1_GPIO_24
#define RX p3  					// NINA_B1_GPIO_25