

    /**
     * Read, Write, Notify, Indicate  Characteristic declaration helper.
     *
     * @tparam T type of data held by the characteristic.
     */
    template<typename T>
    class ReadWriteNotifyIndicateCharacteristic : public GattCharacteristic {
    public:
        /**
         * Construct a characteristic that can be read or written and emit
         * notification or indication.
         *
         * @param[in] uuid The UUID of the characteristic.
         * @param[in] initial_value Initial value contained by the characteristic.
         */
        ReadWriteNotifyIndicateCharacteristic(const UUID & uuid, const T& initial_value) :
            GattCharacteristic(
                /* UUID */ uuid,
                /* Initial value */ &_value,
                /* Value size */ sizeof(_value),
                /* Value capacity */ sizeof(_value),
                /* Properties */ GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ |
                                GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE |
                                GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_NOTIFY |
                                GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_INDICATE,
                /* Descriptors */ NULL,
                /* Num descriptors */ 0,
                /* variable len */ false
            ),
            _value(initial_value) {
        }

        /**
         * Get the value of this characteristic.
         *
         * @param[in] server GattServer instance that contain the characteristic
         * value.
         * @param[in] dst Variable that will receive the characteristic value.
         *
         * @return BLE_ERROR_NONE in case of success or an appropriate error code.
         */
        ble_error_t get(GattServer &server, T& dst) const
        {
            uint16_t value_length = sizeof(dst);
            return server.read(getValueHandle(), &dst, &value_length);
        }

        /**
         * Assign a new value to this characteristic.
         *
         * @param[in] server GattServer instance that will receive the new value.
         * @param[in] value The new value to set.
         * @param[in] local_only Flag that determine if the change should be kept
         * locally or forwarded to subscribed clients.
         */
        ble_error_t set(
            GattServer &server, const uint8_t &value, bool local_only = false
        ) const {
            return server.write(getValueHandle(), &value, sizeof(value), local_only);
        }

    private:
        T _value;
    };


    /**
 * Helper class to construct a readable and writable GattCharacteristic with an array
 * value.
 */
template <typename T, unsigned NUM_ELEMENTS>
class ReadWriteNotifyArrayGattCharacteristic : public GattCharacteristic {
public:
    /**
     * Construct a ReadWriteGattCharacteristic.
     *
     * @param[in] uuid
     *              The characteristic's UUID.
     * @param[in] valuePtr
     *              Pointer to an array of length NUM_ELEMENTS containing the
     *              characteristic's intitial value.
     * @param[in] additionalProperties
     *              Additional characterisitic properties. By default, the
     *              properties are set to
     *              Properties_t::BLE_GATT_CHAR_PROPERTIES_WRITE |
     *              Properties_t::BLE_GATT_CHAR_PROPERTIES_READ.
     * @param[in] descriptors
     *              An array of pointers to descriptors to be added to the new
     *              characteristic.
     * @param[in] numDescriptors
     *              The total number of descriptors in @p descriptors.
     *
     * @note Instances of ReadWriteGattCharacteristic have variable length
     *       attribute value with maximum size equal to sizeof(T) * NUM_ELEMENTS.
     *       For a fixed length alternative use GattCharacteristic directly.
     */
    ReadWriteNotifyArrayGattCharacteristic<T, NUM_ELEMENTS>(const UUID    &uuid,
                                                      T              valuePtr[NUM_ELEMENTS],
                                                      uint8_t        additionalProperties = BLE_GATT_CHAR_PROPERTIES_NONE,
                                                      GattAttribute *descriptors[]        = NULL,
                                                      unsigned       numDescriptors       = 0) :
        GattCharacteristic(uuid, reinterpret_cast<uint8_t *>(valuePtr), sizeof(T) * NUM_ELEMENTS, sizeof(T) * NUM_ELEMENTS,
                           BLE_GATT_CHAR_PROPERTIES_READ | BLE_GATT_CHAR_PROPERTIES_WRITE | additionalProperties, descriptors, numDescriptors) {
        /* empty */
    }
};