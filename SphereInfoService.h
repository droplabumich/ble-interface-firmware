/**
 * A Clock service that demonstrate the GattServer features.
 *
 * The clock service host three characteristics that model the current hour,
 * minute and second of the clock. The value of the second characteristic is
 * incremented automatically by the system.
 *
 * A client can subscribe to updates of the clock characteristics and get
 * notified when one of the value is changed. Clients can also change value of
 * the second, minute and hour characteristric.
 */
class SphereInfoService {
    typedef SphereInfoService Self;

public:
    SphereInfoService(uint8_t* initial_state, float* sphere_state) :
        _power_rails_char("612c867a-07fa-40d3-8e21-afcd96e2109a", initial_state),
        _sphere_status("485f4145-52b9-4644-af1f-7a6b9322490f", sphere_state),
        _power12VAlt_char("0757a65b2-1fab-4225-9726-c9283c71ca56",0),
        
        _clock_service(
            /* uuid */ "51311102-030e-485f-b122-f8f381aa84ed",
            /* characteristics */ _clock_characteristics,
            /* numCharacteristics */ sizeof(_clock_characteristics) /
                                     sizeof(_clock_characteristics[0])
        ),
        _server(NULL),
        _event_queue(NULL)
    {
        // update internal pointers (value, descriptors and characteristics array)
        _clock_characteristics[0] = &_power_rails_char;
        _clock_characteristics[1] = &_sphere_status;
        _clock_characteristics[2] = &_power12VAlt_char;
        

        // setup authorization handlers
        _power_rails_char.setWriteAuthorizationCallback(this, &Self::authorize_client_write);
        
    }



    void start(BLE &ble_interface, events::EventQueue &event_queue)
    {
         if (_event_queue) {
            return;
        }

        _server = &ble_interface.gattServer();
        _event_queue = &event_queue;

        // register the service
        pc.printf("Adding demo service\r\n");
        ble_error_t err = _server->addService(_clock_service);

        if (err) {
            pc.printf("Error %u during demo service registration.\r\n", err);
            return;
        }

        // read write handler
        _server->onDataSent(as_cb(&Self::when_data_sent));
        _server->onDataWritten(as_cb(&Self::when_data_written));
        _server->onDataRead(as_cb(&Self::when_data_read));

        // updates subscribtion handlers
        _server->onUpdatesEnabled(as_cb(&Self::when_update_enabled));
        _server->onUpdatesDisabled(as_cb(&Self::when_update_disabled));
        _server->onConfirmationReceived(as_cb(&Self::when_confirmation_received));

        // print the handles
        pc.printf("clock service registered\r\n");
        pc.printf("service handle: %u\r\n", _clock_service.getHandle());
        pc.printf("\tPower Rail characteristic value handle %u\r\n", _power_rails_char.getValueHandle());
        pc.printf("\tSphere status characteristic value handle %u\r\n", _sphere_status.getValueHandle());
        
        _event_queue->call_every(1000 /* ms */, callback(this, &Self::increment_value));
    }

private:

    /**
     * Handler called when a notification or an indication has been sent.
     */
    void when_data_sent(unsigned count)
    {
        pc.printf("sent %u updates\r\n", count);
    }

    /**
     * Handler called after an attribute has been written.
     */
    void when_data_written(const GattWriteCallbackParams *e)
    {
        pc.printf("data written:\r\n");
        pc.printf("\tconnection handle: %u\r\n", e->connHandle);
        pc.printf("\tattribute handle: %u", e->handle);

        if (e->handle == _power_rails_char.getValueHandle()) {
            power_3v3_enable = e->data[0];
            ThisThread::sleep_for(10);
            power_5v_enable = !e->data[1];
            ThisThread::sleep_for(10);
            esc_enable = e->data[4];
            ThisThread::sleep_for(10);
            altimeter_enable = e->data[3];
            ThisThread::sleep_for(10);
            cameras_enable = e->data[2];
            ThisThread::sleep_for(10);

            pc.printf("Power rails handle written\n\r");
            pc.printf("\t 3V3 value: %i\n\r",e->data[0]);
            pc.printf("\t 5V value: %i\n\r",e->data[1]);
            pc.printf("\t ESC value: %i\n\r",e->data[4]);
            pc.printf("\t 12VAlt value: %i\n\r",e->data[3]);
            pc.printf("\t 12VCam value: %i\n\r",e->data[2]);

        }

        if (e->handle == _sphere_status.getValueHandle()) {
            pc.printf(" (Sphere Status written characteristic)\r\n");
        } else {
            pc.printf("\r\n");
        }
        pc.printf("\twrite operation: %u\r\n", e->writeOp);
        pc.printf("\toffset: %u\r\n", e->offset);
        pc.printf("\tlength: %u\r\n", e->len);
        pc.printf("\t data: ");

        for (size_t i = 0; i < e->len; ++i) {
            pc.printf("%02X", e->data[i]);
        }

        pc.printf("\r\n");
    }

    /**
     * Handler called after an attribute has been read.
     */
    void when_data_read(const GattReadCallbackParams *e)
    {
        pc.printf("data read:\r\n");
        pc.printf("\tconnection handle: %u\r\n", e->connHandle);
        pc.printf("\tattribute handle: %u", e->handle);
        if (e->handle == _sphere_status.getValueHandle()) {
            pc.printf(" (Sphere state characteristic)\r\n");
        } else if (e->handle == _power_rails_char.getValueHandle()) {
            pc.printf(" (Power rails  characteristic)\r\n");
        } else {
            pc.printf("\r\n");
        }
    }

    /**
     * Handler called after a client has subscribed to notification or indication.
     *
     * @param handle Handle of the characteristic value affected by the change.
     */
    void when_update_enabled(GattAttribute::Handle_t handle)
    {
        pc.printf("update enabled on handle %d\r\n", handle);
    }

    /**
     * Handler called after a client has cancelled his subscription from
     * notification or indication.
     *
     * @param handle Handle of the characteristic value affected by the change.
     */
    void when_update_disabled(GattAttribute::Handle_t handle)
    {
        pc.printf("update disabled on handle %d\r\n", handle);
    }

    /**
     * Handler called when an indication confirmation has been received.
     *
     * @param handle Handle of the characteristic value that has emitted the
     * indication.
     */
    void when_confirmation_received(GattAttribute::Handle_t handle)
    {
        pc.printf("confirmation received on handle %d\r\n", handle);
    }

    /**
     * Handler called when a write request is received.
     *
     * This handler verify that the value submitted by the client is valid before
     * authorizing the operation.
     */
    void authorize_client_write(GattWriteAuthCallbackParams *e)
    {
        pc.printf("characteristic %u write authorization\r\n", e->handle);

        if (e->offset != 0) {
            pc.printf("Error invalid offset\r\n");
            e->authorizationReply = AUTH_CALLBACK_REPLY_ATTERR_INVALID_OFFSET;
            return;
        }

        if (e->handle == _power_rails_char.getValueHandle()) {
            if (e->len != 5) {
                pc.printf("Error invalid len\r\n");
                e->authorizationReply = AUTH_CALLBACK_REPLY_ATTERR_INVALID_ATT_VAL_LENGTH;
                return;
            }
            else if ( (e->data[0]>1) || (e->data[1]>1) || (e->data[2]>1) || (e->data[3]>1) || (e->data[4]>1) ) {
                pc.printf("Error invalid data\r\n");
                e->authorizationReply = AUTH_CALLBACK_REPLY_ATTERR_WRITE_NOT_PERMITTED;   
                return;
            }
        } 

        

        e->authorizationReply = AUTH_CALLBACK_REPLY_SUCCESS;
        return;
    }

    void increment_value() {
        uint8_t val; 
        _power12VAlt_char.get(*_server, val);
        val++;
        _power12VAlt_char.set(*_server, val);
    }

private:
   
    /**
     * Helper that construct an event handler from a member function of this
     * instance.
     */
    template<typename Arg>
    FunctionPointerWithContext<Arg> as_cb(void (Self::*member)(Arg))
    {
        return makeFunctionPointer(this, member);
    }

    ReadWriteArrayGattCharacteristic<uint8_t,5> _power_rails_char;
    ReadOnlyArrayGattCharacteristic<float, 10> _sphere_status;

    
    ReadWriteNotifyIndicateCharacteristic<uint8_t> _power12VAlt_char;
    //ReadWriteNotifyIndicateCharacteristic<uint8_t> _powerBatEsc_char;


    // list of the characteristics of the SphereInfo service
    GattCharacteristic* _clock_characteristics[3];

    // demo service
    GattService _clock_service;

    GattServer* _server;
    events::EventQueue *_event_queue;
};