/* mbed Microcontroller Library
 * Copyright (c) 2017 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mbed.h"

#include "nrf_gpio.h"
#include "nrf_delay.h"

#include <stdio.h>

#include "platform/Callback.h"
#include "events/EventQueue.h"
#include "platform/NonCopyable.h"

#include "ble/BLE.h"
#include "ble/Gap.h"
#include "ble/GattClient.h"
#include "ble/GapAdvertisingParams.h"
#include "ble/GapAdvertisingData.h"
#include "ble/GattServer.h"

#include "BLEProcess.h"
#include "BLE_characteristics.h"


#include "CommandInterface.h"
#include "pindefs.h"
SENSOR_DATA::State state;

float sphere_state[10] = {1.1, 3.5, 11000.11, 345.23, 98780.5643, 0.5, 13.78, 5.55, 7.66, 9.88};
uint8_t initial_power_rails_state[6] = {0, 0, 0, 0, 0, 1};

//RawSerial pc(NINA_B1_GPIO_23,NINA_B1_GPIO_22);
RawSerial pc(TX, RX);

SerialProtocol protocol(&state, &pc);

DigitalOut status_led(STATUS_LED);
DigitalOut camera_trigger(CAMERA_TRIGGER);
DigitalOut strobe_trigger(STROBE_TRIGGER);

DigitalOut power_3v3_enable(POWER_3V3_ENABLE);
DigitalOut power_5v_enable(POWER_5V_ENABLE);
DigitalOut esc_enable(ESC_ENABLE);
DigitalOut altimeter_enable(ALTIMETER_ENABLE);
DigitalOut cameras_enable(CAMERAS_ENABLE);


DigitalOut charger_shutdown(CHARGER_SHUTDOWN);
AnalogIn charger_sense(CHARGER_SENSE);

InterruptIn cs(CS);

Timeout camera_trigger_duration;
Thread t1; //using default parameters

using mbed::callback;


/**
 * A Clock service that demonstrate the GattServer features.
 *
 * The clock service host three characteristics that model the current hour,
 * minute and second of the clock. The value of the second characteristic is
 * incremented automatically by the system.
 *
 * A client can subscribe to updates of the clock characteristics and get
 * notified when one of the value is changed. Clients can also change value of
 * the second, minute and hour characteristric.
 */
class SphereInfoService {
    typedef SphereInfoService Self;

public:
    SphereInfoService(uint8_t* initial_state, float* sphere_state) :
        _power_rails_char("612c867a-07fa-40d3-8e21-afcd96e2109a", initial_state),
        _sphere_status("485f4145-52b9-4644-af1f-7a6b9322490f", sphere_state),
        _power12VAlt_char("0757a65b2-1fab-4225-9726-c9283c71ca56",0),
        
        _clock_service(
            /* uuid */ "51311102-030e-485f-b122-f8f381aa84ed",
            /* characteristics */ _clock_characteristics,
            /* numCharacteristics */ sizeof(_clock_characteristics) /
                                     sizeof(_clock_characteristics[0])
        ),
        _server(NULL),
        _event_queue(NULL)
    {
        // update internal pointers (value, descriptors and characteristics array)
        _clock_characteristics[0] = &_power_rails_char;
        _clock_characteristics[1] = &_sphere_status;
        _clock_characteristics[2] = &_power12VAlt_char;
        

        // setup authorization handlers
        _power_rails_char.setWriteAuthorizationCallback(this, &Self::authorize_client_write);
        

    }



    void start(BLE &ble_interface, events::EventQueue &event_queue)
    {
         if (_event_queue) {
            return;
        }

        _server = &ble_interface.gattServer();
        _event_queue = &event_queue;

        // register the service
        pc.printf("Adding demo service\r\n");
        ble_error_t err = _server->addService(_clock_service);

        if (err) {
            pc.printf("Error %u during demo service registration.\r\n", err);
            return;
        }

        // read write handler
        _server->onDataSent(as_cb(&Self::when_data_sent));
        _server->onDataWritten(as_cb(&Self::when_data_written));
        _server->onDataRead(as_cb(&Self::when_data_read));

        // updates subscribtion handlers
        _server->onUpdatesEnabled(as_cb(&Self::when_update_enabled));
        _server->onUpdatesDisabled(as_cb(&Self::when_update_disabled));
        _server->onConfirmationReceived(as_cb(&Self::when_confirmation_received));

        // print the handles
        pc.printf("clock service registered\r\n");
        pc.printf("service handle: %u\r\n", _clock_service.getHandle());
        pc.printf("\tPower Rail characteristic value handle %u\r\n", _power_rails_char.getValueHandle());
        pc.printf("\tSphere status characteristic value handle %u\r\n", _sphere_status.getValueHandle());
        
        _event_queue->call_every(500 /* ms */, callback(this, &Self::update_sphere_state));
    }

  

private:

    /**
     * Handler called when a notification or an indication has been sent.
     */
    void when_data_sent(unsigned count)
    {
        pc.printf("sent %u updates\r\n", count);
    }

    /**
     * Handler called after an attribute has been written.
     */
    void when_data_written(const GattWriteCallbackParams *e)
    {
        pc.printf("data written:\r\n");
        pc.printf("\tconnection handle: %u\r\n", e->connHandle);
        pc.printf("\tattribute handle: %u", e->handle);

        if (e->handle == _power_rails_char.getValueHandle()) {
            power_3v3_enable = e->data[0];
            ThisThread::sleep_for(10);
            power_5v_enable = !e->data[1];
            ThisThread::sleep_for(10);
            esc_enable = e->data[4];
            ThisThread::sleep_for(10);
            altimeter_enable = e->data[3];
            ThisThread::sleep_for(10);
            cameras_enable = e->data[2];
            ThisThread::sleep_for(10);
            charger_shutdown = !e->data[5];


            pc.printf("Power rails handle written\n\r");
            pc.printf("\t 3V3 value: %i\n\r",e->data[0]);
            pc.printf("\t 5V value: %i\n\r",e->data[1]);
            pc.printf("\t ESC value: %i\n\r",e->data[4]);
            pc.printf("\t 12VAlt value: %i\n\r",e->data[3]);
            pc.printf("\t 12VCam value: %i\n\r",e->data[2]);
            pc.printf("\t Charger Enabled: %i\n\r",e->data[5]);

        }

        if (e->handle == _sphere_status.getValueHandle()) {
            pc.printf(" (Sphere Status written characteristic)\r\n");
        } else {
            pc.printf("\r\n");
        }
        pc.printf("\twrite operation: %u\r\n", e->writeOp);
        pc.printf("\toffset: %u\r\n", e->offset);
        pc.printf("\tlength: %u\r\n", e->len);
        pc.printf("\t data: ");

        for (size_t i = 0; i < e->len; ++i) {
            pc.printf("%02X", e->data[i]);
        }

        pc.printf("\r\n");
    }

    /**
     * Handler called after an attribute has been read.
     */
    void when_data_read(const GattReadCallbackParams *e)
    {
        pc.printf("data read:\r\n");
        pc.printf("\tconnection handle: %u\r\n", e->connHandle);
        pc.printf("\tattribute handle: %u", e->handle);
        if (e->handle == _sphere_status.getValueHandle()) {
            pc.printf(" (Sphere state characteristic)\r\n");
        } else if (e->handle == _power_rails_char.getValueHandle()) {
            pc.printf(" (Power rails  characteristic)\r\n");
        } else {
            pc.printf("\r\n");
        }
    }

    /**
     * Handler called after a client has subscribed to notification or indication.
     *
     * @param handle Handle of the characteristic value affected by the change.
     */
    void when_update_enabled(GattAttribute::Handle_t handle)
    {
        pc.printf("update enabled on handle %d\r\n", handle);
    }

    /**
     * Handler called after a client has cancelled his subscription from
     * notification or indication.
     *
     * @param handle Handle of the characteristic value affected by the change.
     */
    void when_update_disabled(GattAttribute::Handle_t handle)
    {
        pc.printf("update disabled on handle %d\r\n", handle);
    }

    /**
     * Handler called when an indication confirmation has been received.
     *
     * @param handle Handle of the characteristic value that has emitted the
     * indication.
     */
    void when_confirmation_received(GattAttribute::Handle_t handle)
    {
        pc.printf("confirmation received on handle %d\r\n", handle);
    }

    /**
     * Handler called when a write request is received.
     *
     * This handler verify that the value submitted by the client is valid before
     * authorizing the operation.
     */
    void authorize_client_write(GattWriteAuthCallbackParams *e)
    {
        pc.printf("characteristic %u write authorization\r\n", e->handle);

        if (e->offset != 0) {
            pc.printf("Error invalid offset\r\n");
            e->authorizationReply = AUTH_CALLBACK_REPLY_ATTERR_INVALID_OFFSET;
            return;
        }

        if (e->handle == _power_rails_char.getValueHandle()) {
            if (e->len != 6) {
                pc.printf("Error invalid len\r\n");
                e->authorizationReply = AUTH_CALLBACK_REPLY_ATTERR_INVALID_ATT_VAL_LENGTH;
                return;
            }
            else if ( (e->data[0]>1) || (e->data[1]>1) || (e->data[2]>1) || (e->data[3]>1) || (e->data[4]>1) || (e->data[5]>1)) {
                pc.printf("Error invalid data\r\n");
                e->authorizationReply = AUTH_CALLBACK_REPLY_ATTERR_WRITE_NOT_PERMITTED;   
                return;
            }
        } 

        

        e->authorizationReply = AUTH_CALLBACK_REPLY_SUCCESS;
        return;
    }

    void increment_value() {
        uint8_t val; 
        _power12VAlt_char.get(*_server, val);
        val++;
        _power12VAlt_char.set(*_server, val);
    }

    void update_sphere_state() {

        ble_error_t err = _server->write (_sphere_status.getValueHandle(), 
            reinterpret_cast<uint8_t *>(sphere_state), 
            sizeof(float) * 10, false);
        
        if (err) {
            printf("write of the hour value returned error %u\r\n", err);
            return;
        }
    }

private:
   
    /**
     * Helper that construct an event handler from a member function of this
     * instance.
     */
    template<typename Arg>
    FunctionPointerWithContext<Arg> as_cb(void (Self::*member)(Arg))
    {
        return makeFunctionPointer(this, member);
    }

    ReadWriteArrayGattCharacteristic<uint8_t,6> _power_rails_char;
    ReadOnlyArrayGattCharacteristic<float, 10> _sphere_status;

    
    ReadWriteNotifyIndicateCharacteristic<uint8_t> _power12VAlt_char;
    //ReadWriteNotifyIndicateCharacteristic<uint8_t> _powerBatEsc_char;


    // list of the characteristics of the SphereInfo service
    GattCharacteristic* _clock_characteristics[3];

    // demo service
    GattService _clock_service;

    GattServer* _server;
    events::EventQueue *_event_queue;
};


/*! \brief Set up watchdog timer
 *
 *  The watchdog timer is set to 6 seconds
 */
void wdt_init(void)
{
    NRF_WDT->CONFIG = (WDT_CONFIG_HALT_Pause << WDT_CONFIG_HALT_Pos) | ( WDT_CONFIG_SLEEP_Run << WDT_CONFIG_SLEEP_Pos);   //Configure Watchdog. a) Pause watchdog while the CPU is halted by the debugger.  b) Keep the watchdog running while the CPU is sleeping.
    NRF_WDT->CRV = 6*32768;             //ca 3 sek. timout
    NRF_WDT->RREN |= WDT_RREN_RR0_Msk;  //Enable reload register 0
    NRF_WDT->TASKS_START = 1;           //Start the Watchdog timer
}

/* \brief Reset the watchdog timer
 * 
 * This function has to be called regularly to restart the watchdog timer and avoid reset
 */
 void wdt_kick(void) {
    NRF_WDT->RR[0] = WDT_RR_RR_Reload;  //Reload watchdog register 0
}

void protocol_update() {
    status_led != status_led;
    //pc.printf("Executed callback\n\r"); 
    wdt_kick();
    if ( protocol.newMessagereceived()){ //&& ble.getGapState().connected) {
            protocol.parse(protocol.lastMessage());             
    }
}

uint32_t onAD(uint32_t myarg){ 
    pc.printf("Got AD ");
        
    for(int i=0; i<DATA_TYPES::NumberOfTypes; i++){
        pc.printf("Element ID: %i, Value: %8.2f \n\r",i,state.sensors[i].value);
    }
    
    return 0;
}

uint32_t onDM(uint32_t myarg){ 
    pc.printf("Got DM %i with value: %8.2f",myarg,state.sensors[myarg].value);
    return 0;

}

void onSerialRx() {
    while(pc.readable()) {
        char c = pc.getc();
        protocol.process_char(c); 
    }
}

/* Read battery sense voltage and compute corresponding battery charge current 
 * 
 *
 */
void check_charger() {
    float Vibat = charger_sense.read()*3.3;
    sphere_state[4] = Vibat/(0.9*0.001*0.04*8000);
}




void initialize() {
    power_3v3_enable = 0;
    power_5v_enable = 1;
    esc_enable = 0;
    altimeter_enable = 0;
    cameras_enable = 0;    
    charger_shutdown = 1; 
    camera_trigger = 0;
}


void reset_camera_trigger() {
    camera_trigger = 0;
}

void trigger_camera() {
    camera_trigger = 1;
    camera_trigger_duration.attach(&reset_camera_trigger,0.005);
}





int main() {
    pc.baud(115200);
    pc.set_flow_control(Serial::Disabled);
    pc.printf("Started \n\r");
    pc.attach(&onSerialRx);
    
    initialize();
    cs.mode(PullUp);
    cs.fall(&trigger_camera);

    protocol.attach(SerialProtocol::AD,&onAD);
    protocol.attach(SerialProtocol::DM,&onDM);

    wdt_init();
    // Int temperature, pressure, humidity, status and battery 

    BLE &ble_interface = BLE::Instance();
    events::EventQueue event_queue;


    BLEProcess ble_process(event_queue, ble_interface);
    SphereInfoService demo_service(initial_power_rails_state, sphere_state);

    ble_process.on_init(callback(&demo_service, &SphereInfoService::start));

    // bind the event queue to the ble interface, initialize the interface
    // and start advertising
    ble_process.start();

    event_queue.call_every(500, protocol_update);
    //event_queue.call_every(250, trigger_camera); // Trigger at 4Hz. 
    event_queue.call_every(50, check_charger);   

    // Process the event queue.
    event_queue.dispatch_forever();

    return 0;
}
